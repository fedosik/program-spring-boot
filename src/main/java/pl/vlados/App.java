package pl.vlados;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

/**
 * Mamy dany program przetwarzający rekordy danych, których składowe można przedstawić w postaci par nazwa pola-wartość (np. srcIp="192.168.10.1").
 *
 * Każdy rekord posiada pole czas, które jest wymagane. Rekordy mogą reprezentować dane różnego typu np. wpisy z logów.
 *
 * Rekordy mogą być odczytywane z dowolnego źródła (np. plik, baza danych) i zapisywane do dowolnego celu (np. plik, baza danych).
 *
 * Zakładamy, że w każdej chwili może wystąpić konieczność dodania nowej implementacji źródła lub celu.
 *
 * Zadania:
 *
 * 1. Proszę zaprojektować interfejsy i klasy (bez szczegółowej implementacji) dla programu spełniającego powyższe założenia.
 *
 * 2. Przy użyciu definicji z punktu 1., proszę zaimplementować w pełni działający zapis rekordów do bazy MongoDB.
 *
 * 3. Proszę zaimplementować testy jednostkowe sprawdzające poprawność implementacji z punktu 2.
 *
 */
@SpringBootApplication
public class App 
{
    public static void main( String[] args )
    {
        new SpringApplicationBuilder(App.class).web(false).run(args);
    }
}
