package pl.vlados.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.vlados.sources.SourceLocal;
import pl.vlados.sources.SourceMongo;

@Configuration
public class MyConfig {

        @Bean(name = "sourceLocal")
        public SourceLocal getSourceLocal(){
            return new SourceLocal();
        }

        @Bean(name = "sourceMongo")
        public SourceMongo getSourceMongo(){
            return new SourceMongo();
        }
}
