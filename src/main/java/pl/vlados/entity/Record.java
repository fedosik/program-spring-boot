package pl.vlados.entity;

import org.springframework.data.mongodb.core.mapping.Document;
import java.math.BigInteger;
import java.util.Date;

@Document
public class Record {

    private BigInteger id;

    private Date createdTime;

    private String key;

    private String value;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Record{" +
                "createdTime=" + createdTime +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                '}';
    }

    public String getAsPairFormat(){
        return getKey() + "=" + getValue();
    }
}
