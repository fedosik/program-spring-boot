package pl.vlados.persistence;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import pl.vlados.entity.Record;

@Repository
public interface RecordRepository extends MongoRepository<Record, String> {

}
