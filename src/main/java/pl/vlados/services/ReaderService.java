package pl.vlados.services;

import javafx.util.Pair;
import pl.vlados.entity.Record;
import java.util.List;

public interface ReaderService {


    Pair<String, String> parseStringToPair(String record);

    List<Record> getResultObject();
}
