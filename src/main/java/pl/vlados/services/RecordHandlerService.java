package pl.vlados.services;

import pl.vlados.entity.Record;
import java.util.List;

public interface RecordHandlerService {

    List<Record> read();

    List<Record> write();
}
