package pl.vlados.services;

import pl.vlados.entity.Record;
import java.util.List;

public interface SourceService {

    List<String> getRecords();

    List<Record> writeRecords(List<Record> record);

}
