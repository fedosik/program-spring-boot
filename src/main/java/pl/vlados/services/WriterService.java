package pl.vlados.services;

import pl.vlados.entity.Record;
import java.util.List;

public interface WriterService {

    List<Record> writeRecordToSource(List<Record> records);
}
