package pl.vlados.sources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import pl.vlados.entity.Record;
import pl.vlados.persistence.RecordRepository;
import pl.vlados.services.SourceService;
import java.util.List;

public class SourceMongo implements SourceService {

    @Autowired
    private RecordRepository recordRepository;

    @Override
    public List<String> getRecords() {
        //todo
        return null;
    }

    @Transactional
    public List<Record> writeRecords(List<Record> record) {

        if(record == null){
            throw new NullPointerException("Record is null");
        }

        List<Record> records = recordRepository.save(record);

        System.out.println("The followings records : ");

        for(Record rec : records){
            System.out.println(rec.toString());
        }

        System.out.println("have added to data base ");

        return records;
    }
}
