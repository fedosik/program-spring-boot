package pl.vlados;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.vlados.entity.Record;
import pl.vlados.services.RecordHandlerService;

import java.util.List;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest 
{
    @Autowired
    private RecordHandlerService recordHandlerService;

    @Test
    public void addRecordToDb(){

        List<Record> records = null;
        try{
            records = recordHandlerService.write();
        }catch (Exception e){
            e.printStackTrace();
        }

        Assert.assertNotNull(records);
        Assert.assertNotEquals(0, records.size());
    }
}
