package pl.vlados.services;

import javafx.util.Pair;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.vlados.entity.Record;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReaderServiceImplTest implements ReaderService {

    private SourceService source;

    ReaderServiceImplTest(@Qualifier("sourceLocal") SourceService source){
        this.source = source;
    }

    @Override
    public Pair<String, String> parseStringToPair(String record) {

        //parsujemy string do pary
        if(record == null){
            throw new NullPointerException("Record is null");
        }

        if(!record.contains("=")){
            throw new IllegalArgumentException("Nieprawidlowy format rekordu");
        }

        Long count = record.chars().filter(num -> num == '=').count();
        if(count > 1){
            throw new IllegalArgumentException("Znak rownosci musi byc tylko jeden");
        }

        String key = record.split("=")[0];
        String value = record.split("=")[1];

        return new Pair(key,value);
    }

    @Override
    public List<Record> getResultObject() {

        if(source.getRecords() == null){
            throw new NullPointerException();
        }

        //tworzymy obiekty pomocnicze na podstawie danych ze zrodla
        List<Record> list = new ArrayList<>();

        for(String s : source.getRecords()) {
            Pair<String, String> pair = parseStringToPair(s);

            Record record = new Record();
            record.setCreatedTime(new Date());
            record.setKey(pair.getKey());
            record.setValue(pair.getValue());
            list.add(record);
        }
        return list;
    }
}
