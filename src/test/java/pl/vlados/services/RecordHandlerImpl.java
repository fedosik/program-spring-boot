package pl.vlados.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.vlados.entity.Record;
import java.util.List;

@Service
public class RecordHandlerImpl implements RecordHandlerService {

    @Autowired
    ReaderService readerService;

    @Autowired
    WriterService writerService;

    @Override
    public List<Record> read() {

        List<Record> records = readerService.getResultObject();
        System.out.println(records.toString());

        return records;
    }

    @Override
    public List<Record> write() {
        return writerService.writeRecordToSource(read());
    }
}
