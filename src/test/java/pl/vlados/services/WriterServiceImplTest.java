package pl.vlados.services;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.vlados.entity.Record;
import java.util.List;

@Service
public class WriterServiceImplTest implements WriterService {

    private SourceService source;

    WriterServiceImplTest(@Qualifier("sourceMongo") SourceService source){
        this.source = source;
    }

    public List<Record> writeRecordToSource(List<Record> records) {

        if(records == null){
            throw new NullPointerException();
        }

        return source.writeRecords(records);
    }
}